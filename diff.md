```diff
     RatType:
-      anyOf:
-        - type: string
-          enum:
-            - NR
-            - EUTRA
-            - WLAN
-            - VIRTUAL
+      type: string
+      enum:
+        - NR
+        - EUTRA
+        - WLAN
+        - VIRTUAL
     RestrictionType:
-      anyOf:
-        - type: string
-          nullable: true
-          enum:
-            - ALLOWED_AREAS
-            - NOT_ALLOWED_AREAS
-        - type: string
+      type: string
+      nullable: true
+      enum:
+        - ALLOWED_AREAS
+        - NOT_ALLOWED_AREAS
     CoreNetworkType:
-      anyOf:
-        - type: string
-          enum:
-            - 5GC
-            - EPC
+      type: string
+      enum:
+        - 5GC
+        - EPC
```
