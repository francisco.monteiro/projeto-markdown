# Process 1

```mermaid
flowchart TD
    A[Start] --> B{Is it?}
    B -->|Yes| C[OK]
    C --> D[Rethink]
    D --> B
    B ---->|No| E[End]
```

## Algorithm
When something is done we can rethink and redo, otherwise there's nothing to rethink :P